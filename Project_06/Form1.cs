﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_06
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        string sqlConn = @"Server=DESKTOP-IIQ7LRK\SQLEXPRESS;Database=CSDLQuanLySinhVien;Integrated Security = True";
        SqlConnection conn = null;

        private void OpenConnection()
        {
            if (conn == null)
                conn = new SqlConnection(sqlConn);
            if (conn.State == ConnectionState.Closed)
                conn.Open();
        }

        private void ClosedConnection()
        {
            if (conn != null && conn.State == ConnectionState.Open)
                conn.Close();
        }

        private void HienThiToanBoSinhVien()
        {
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = "select * from SinhVien order by ID";
                command.Connection = conn;
                SqlDataReader reader = command.ExecuteReader();
                lvSinhVien.Items.Clear();
                while (reader.Read())
                {
                    string id = reader.GetString(0);
                    string fullName = reader.GetString(1);
                    int gender = reader.GetInt32(2);
                    string phone = reader.GetString(3);
                    string address = reader.GetString(4);
                    ListViewItem lvi = new ListViewItem(id);
                    lvi.SubItems.Add(fullName);
                    lvi.SubItems.Add(gender == 0 ? "Nam" : "Nữ");
                    lvi.SubItems.Add(phone);
                    lvi.SubItems.Add(address);
                    lvSinhVien.Items.Add(lvi);
                    if (gender == 0)
                        lvi.ImageIndex = 0;
                    else
                        lvi.ImageIndex = 1;
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            HienThiToanBoSinhVien();
        }

        private void HienThiChiTiet(string id)
        {
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = "select * from SinhVien where ID=@id";
                command.Connection = conn;
                SqlParameter parameter = new SqlParameter("@id", SqlDbType.NVarChar);
                parameter.Value = id;
                command.Parameters.Add(parameter);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    string fullName = reader.GetString(1);
                    int gender = reader.GetInt32(2);
                    string phone = reader.GetString(3);
                    string address = reader.GetString(4);
                    txtID.Text = id;
                    txtFullName.Text = fullName;
                    if (gender == 0)
                        radMale.Checked = true;
                    else
                        radFemale.Checked = true;
                    txtPhone.Text = phone;
                    txtAddress.Text = address;
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lvSinhVien_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvSinhVien.SelectedItems.Count == 0) return;
            if (lvSinhVien.SelectedItems.Count > 0)
            {
                ListViewItem lvi = lvSinhVien.SelectedItems[0];
                string id = lvi.Text;
                //string id = lvSinhVien.SelectedItems[0].SubItems[0].Text;
                HienThiChiTiet(id);
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            SetErrorProvider();
            txtID.Text = "";
            txtFullName.Text = "";
            txtPhone.Text = "";
            radMale.Checked = false;
            radFemale.Checked = false;
            txtAddress.Text = "";
            txtID.Focus();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SetErrorProvider();
            if (txtID.Text == "" || txtFullName.Text == "" || txtID.Text == "" || txtAddress.Text == "" || (radMale.Checked == false && radFemale.Checked == false))
            {
                MessageBox.Show("Vui lòng nhập đầy đủ thông tin của bạn !");
                if (txtID.Text == "")
                    errorProvider1.SetError(txtID, "Vui lòng nhập MSSV !");
                if (txtFullName.Text == "")
                    errorProvider1.SetError(txtFullName, "Vui lòng nhập họ tên !");
                if (txtPhone.Text == "")
                    errorProvider1.SetError(txtPhone, "Vui lòng nhập số điện thoại !");
                if (txtAddress.Text == "")
                    errorProvider1.SetError(txtAddress, "Vui lòng nhập địa chỉ !");
                if (radMale.Checked == false && radFemale.Checked == false)
                    errorProvider1.SetError(radFemale, "Vui lòng chọn giới tính !");
            }
            else
            {
                // Kiểm tra dữ liệu nhập vào
                Regex regexID = new Regex(@"^15|16|17|18|19|20H|h\\d+$");
                bool resultID = regexID.IsMatch(txtID.Text);
                Regex regexPhone = new Regex(@"^03|09\d+$");
                bool resultPhone = regexPhone.IsMatch(txtPhone.Text);
                Regex regexAddress = new Regex("^[0-9]+$");
                bool resultAddress = regexAddress.IsMatch(txtAddress.Text);
                Regex regexFullName = new Regex("^[0-9]+$");
                bool resultFullName = regexFullName.IsMatch(txtFullName.Text);
                if ((resultID && txtID.Text.Length == 10)
                    && (resultPhone && txtPhone.Text.Length == 10)
                    && !resultAddress && !resultFullName
                    && (radMale.Checked || radFemale.Checked))
                {
                    string id = txtID.Text;
                    if (KiemTraTonTai(id)) // Nếu thông tin sinh viên tồn tại thì cập nhật mới
                    {
                        //tiến hành cập nhật
                        CapNhatKhachHang(id);
                    }
                    else
                    {
                        // tiến hành thêm mới
                        ThemMoiKhachHang();
                    }

                }
                else
                {
                    MessageBox.Show("Thêm không thành công ! Vui lòng nhập đúng thông tin.");
                    if (!resultID && txtID.Text.Length != 10)
                        errorProvider1.SetError(txtID, "Vui lòng nhập đúng MSSV !");
                    if (!resultPhone && txtPhone.Text.Length != 10)
                        errorProvider1.SetError(txtPhone, "Vui lòng nhập đúng số điên thoại !");
                    if (resultAddress)
                        errorProvider1.SetError(txtAddress, "Vui lòng đúng địa chỉ !");
                    if (resultFullName)
                        errorProvider1.SetError(txtFullName, "Vui lòng nhập đúng họ tên !");
                }

            }
        }

        private void SetErrorProvider()
        {
            errorProvider1.SetError(txtID, "");
            errorProvider1.SetError(txtFullName, "");
            errorProvider1.SetError(radFemale, "");
            errorProvider1.SetError(txtPhone, "");
            errorProvider1.SetError(txtAddress, "");
        }

        private void ThemMoiKhachHang()
        {
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand();
                command.CommandType = CommandType.Text;
                string sql = "insert into SinhVien values(@id,@fullName,@gender,@phone,@address)";
                command.CommandText = sql;
                command.Connection = conn;
                command.Parameters.Add("@id", SqlDbType.NVarChar).Value = txtID.Text;
                command.Parameters.Add("@fullName", SqlDbType.NVarChar).Value = txtFullName.Text;
                if (radMale.Checked == true)
                    command.Parameters.Add("@gender", SqlDbType.Int).Value = 0;
                else
                    command.Parameters.Add("@gender", SqlDbType.Int).Value = 1;
                command.Parameters.Add("@phone", SqlDbType.NVarChar).Value = txtPhone.Text;
                command.Parameters.Add("@address", SqlDbType.NVarChar).Value = txtAddress.Text;

                int kq = command.ExecuteNonQuery();
                if (kq > 0)
                {
                    HienThiToanBoSinhVien();
                    MessageBox.Show("Lưu thông tin thành công !");
                }
                else
                {
                    MessageBox.Show("Lưu thông tin thất bại !");
                }
                btnNew.PerformClick();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CapNhatKhachHang(string id)
        {
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand();
                command.CommandType = CommandType.Text;
                string sql = "update SinhVien set FullName=@fullName,Gender=@gender,Phone=@phone,Address=@address where ID=@id";
                command.CommandText = sql;
                command.Connection = conn;
                command.Parameters.Add("@fullName", SqlDbType.NVarChar).Value = txtFullName.Text;
                command.Parameters.Add("@gender", SqlDbType.Int).Value = radMale.Checked ? 0 : 1;
                command.Parameters.Add("@phone", SqlDbType.NVarChar).Value = txtPhone.Text;
                command.Parameters.Add("@address", SqlDbType.NVarChar).Value = txtAddress.Text;
                command.Parameters.Add("@id", SqlDbType.NVarChar).Value = txtID.Text;
                int kq = command.ExecuteNonQuery();
                if (kq > 0)
                {
                    HienThiToanBoSinhVien();
                    MessageBox.Show("Cập nhật thành công !");
                    btnNew.PerformClick();
                }
                else
                {
                    MessageBox.Show("Cập nhật thất bại !");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private bool KiemTraTonTai(string id)
        {
            try
            {
                OpenConnection();
                SqlCommand command = new SqlCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = "select * from SinhVien where ID=@id";
                command.Connection = conn;
                SqlParameter parameter = new SqlParameter("@id", SqlDbType.NVarChar);
                parameter.Value = id;
                command.Parameters.Add(parameter);
                SqlDataReader reader = command.ExecuteReader();
                bool kq = reader.Read();
                reader.Close();
                return kq;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return false;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lvSinhVien.SelectedItems.Count == 0)
            {
                MessageBox.Show("Vui lòng chọn sinh viên cần xóa !");
            }
            else
            {
                ListViewItem lvi = lvSinhVien.SelectedItems[0];
                string id = lvi.Text;
                DialogResult ret = MessageBox.Show($"Bạn có chắc chắn muốn xóa Sinh Viên có mã = [{id}] không ?", "Xác nhận xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (ret == DialogResult.Yes)
                {
                    ThucHienXoa(id);
                }
            }
        }

        private void ThucHienXoa(string id)
        {
            try
            {
                OpenConnection(); //Mở kết nối với sql server
                SqlCommand command = new SqlCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = "delete from SinhVien where ID=@id";
                command.Connection = conn;
                command.Parameters.Add("@id", SqlDbType.NVarChar).Value = id;
                int kq = command.ExecuteNonQuery();
                if (kq > 0)
                {
                    HienThiToanBoSinhVien();
                    MessageBox.Show("Xóa sinh viên thành công !");
                    btnNew.PerformClick();
                }
                else
                {
                    MessageBox.Show("Xóa sinh viên thất bại !");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtID_TextChanged(object sender, EventArgs e)
        {
            Regex regexID = new Regex(@"^15|16|17|18|19|20H|h\\d+$"); //Chỉ nhận MSSV viện clc. 
            bool resultID = regexID.IsMatch(txtID.Text);
            if (resultID && txtID.Text.Length == 10) //VD: 19h1120035
            {
                errorProvider1.SetError(txtID, "");
            }
            else
            {
                errorProvider1.SetError(txtID, "Vui lòng nhập đúng mã số sinh viên của bạn !");
            }
            if (txtID.Text == "")
            {
                errorProvider1.SetError(txtID, "");

            }
        }

        private void txtPhone_TextChanged(object sender, EventArgs e)
        {
            Regex regexPhone = new Regex(@"^03|09\d+$"); // chỉ nhận đầu số 03 or 09
            bool resultPhone = regexPhone.IsMatch(txtPhone.Text);
            if (resultPhone && txtPhone.Text.Length == 10) // Nếu nhập đúng và độ dài của phone = 10 thì OK
            {
                errorProvider1.SetError(txtPhone, "");
            }
            else
            {
                errorProvider1.SetError(txtPhone, "Vui lòng nhập đúng số điện thoại ! Chỉ nhận đầu số 03,09");
            }
            if (txtPhone.Text == "")
            {
                errorProvider1.SetError(txtPhone, "");

            }
        }

        private void txtAddress_TextChanged(object sender, EventArgs e)
        {
            Regex regexAddress = new Regex("^[0-9]+$");
            bool resultAddress = regexAddress.IsMatch(txtAddress.Text);
            if (!resultAddress) // nếu nhập khác chữ số thì OK
            {
                errorProvider1.SetError(txtAddress, "");
            }
            else
            {
                errorProvider1.SetError(txtAddress, "Vui lòng nhập đúng địa chỉ.");
            }
        }

        private void txtFullName_TextChanged(object sender, EventArgs e)
        {
            Regex regexFullName = new Regex("^[0-9]+$");
            bool resultFullName = regexFullName.IsMatch(txtFullName.Text);
            if (!resultFullName) // nếu nhập khác chữ số thì OK
            {
                errorProvider1.SetError(txtFullName, "");
            }
            else
            {
                errorProvider1.SetError(txtFullName, "Vui lòng nhập đúng họ và tên.");
            }
        }

        private void radFemale_CheckedChanged(object sender, EventArgs e)
        {
            if (!radMale.Checked && !radFemale.Checked) // nếu 2 checkbox không được click thì hiển thị lỗi
                errorProvider1.SetError(radFemale, "Vui lòng chọn giới tính !");
            errorProvider1.SetError(radFemale, "");
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Đóng chương trình
            DialogResult ret = MessageBox.Show("Bạn có chắc chắn muốn thoát chương trình không ?",
                "Xác nhận thoát", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (ret == DialogResult.No) // nếu ret = No thì hủy thoát
            {
                e.Cancel = true;
            }
        }
    }
}
