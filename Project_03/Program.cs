﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace Project_03
{
    class Program
    {
        static void Main(string[] args)
        {
            //Dns: cung cấp chức năng phân giải tên miền đơn giản
            //GetHostName(): lấy tên máy chủ của máy tính
            string hostName = Dns.GetHostName();
            Console.WriteLine($"Local hostname: {hostName}");
            IPHostEntry myself = Dns.GetHostByName(hostName);
            foreach (IPAddress address in myself.AddressList)
            {
                Console.WriteLine($"IP address: {address.ToString()}");
            }
        }
    }
}
