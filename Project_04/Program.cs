﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_04
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            // khai báo đối tượng SaveFileDialog để chọn vị trí lưu file
            SaveFileDialog saveDialog = new SaveFileDialog();
            // Đặt tùy chọn bộ lọc và chỉ muc lọc : giới hạn đối với file ảnh .png
            saveDialog.Filter = "PNG Files (.png)|*.png|All Files (*.*)|*.*";
            //đặt chỉ mục của bộ lọc hiện được chọn trong tệp : PNG Files (.png)|*.png
            saveDialog.FilterIndex = 1;
            Console.WriteLine("Nhập vào kich thước của ảnh: ");
            Console.Write("Nhập vào chiêu rông của ảnh: ");
            int width = int.Parse(Console.ReadLine());
            Console.Write("Nhập vào chiều cao của ảnh: ");
            int height = int.Parse(Console.ReadLine());

            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                // nếu click ok thì xử lý chụp màn hình

                //khởi tạo một phiên bản mới của lớp Bitmap với kích thước và định dạng được chỉ định 
                // Screen.PrimaryScreen.Bounds.Width: chiều rộng hiển thị của màn hình
                // Screen.PrimaryScreen.Bounds.Height: chiều cao hiển thị của màn hình
                // PixelFormat.Format32bppArgb: chỉ định định dạng của dữ liệu màu cho mỗi pixel trong hình ảnh 
                //Bitmap bmp = new Bitmap(Screen.PrimaryScreen.Bounds.Width,
                //                            Screen.PrimaryScreen.Bounds.Height,
                //                            PixelFormat.Format32bppArgb);
                Bitmap bmp = new Bitmap(width,height,PixelFormat.Format32bppArgb);
                // tạo đồ họa mới từ hình ảnh được chỉ định
                Graphics graphics = Graphics.FromImage(bmp); // trả về 1 đồ họa mới cho hình ảnh được chỉ định
                //chuyển dữ liệu màu theo khối bit, tương ứng với một pixel hình chữ nhật, từ màn hình sang bề mặt vẽ của đồ họa
                graphics.CopyFromScreen(Screen.PrimaryScreen.Bounds.X, //đặt tọa độ x của góc trên bên trái của cấu trúc hình chữ nhật này, mặc định là 0
                    Screen.PrimaryScreen.Bounds.Y,//đặt tọa độ Y của góc trên bên trái của cấu trúc hình chữ nhật này, mặc định là 0
                    0, 0, 
                    Screen.PrimaryScreen.Bounds.Size, // đặt kích thước của hình ảnh
                    CopyPixelOperation.SourceCopy);
                //graphics.CopyFromScreen(0, 0, 0, 0, s);
                // lưu hình ảnh này vào tệp được chỉ định ở định dạng được chỉ định
                bmp.Save(saveDialog.FileName, ImageFormat.Png);
                Console.WriteLine("Chụp màn hình thành công !");
                Console.WriteLine("Ảnh lưu tại: "+saveDialog.FileName);
            }
        }
    }
}