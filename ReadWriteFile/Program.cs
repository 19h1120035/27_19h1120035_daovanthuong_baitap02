﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
 https://docs.microsoft.com/en-us/troubleshoot/dotnet/csharp/read-write-text-file
 */
namespace ReadWriteFile
{
        class Program
        {
            static void Main(string[] args)
            {
                string str;
                int a = 0, b = 0, t;

                Console.OutputEncoding = Encoding.Unicode;
                Console.InputEncoding = Encoding.Unicode;
                Console.WriteLine("C# Read/Write File!");
                ReadFile(ref a, ref b); // đọc file và lấy giá trị a và b
                t = a + b;
                str = "Tổng = " + t;
                WriteFile(str); // ghi chuỗi str vào file
            }

            public static void ReadFile(ref int a, ref int b)
            {
                String line;
                try
                {
                    // khai báo đối tượng Streamreader và truyền vào file cần đọc
                    StreamReader sr = new StreamReader("files/read.txt");
                    line = sr.ReadLine(); // đọc 1 dòng trong file
                    while (line != null) // line == null thì dừng lặp
                    {
                        Console.WriteLine(line);
                        // line.Substring(0,1) : (a=1) => line.Substring(0,1) = a
                        // => 0: lấy chuỗi con bắt đầu từ kí tự đầu tiên
                        // => 1: lấy số kí tự
                        // so sánh chuỗi con của line với "a"
                        if (String.Compare(line.Substring(0, 1), "a", true) == 0) // true: so sánh với các kí tự thường
                        {
                            // nếu giống nhau thì 
                            //line.Substring(2): lấy chuỗi con bắt đầu từ kí tự thứ 2
                            // .trim(): bỏ cái kí tự khoảng trắng ở đầu và cuối của chuỗi
                            a = int.Parse(line.Substring(2).Trim()); // a = 1
                        }
                        else // (b=2)
                        {
                            b = int.Parse(line.Substring(2).Trim()); // b = 2
                        }
                        line = sr.ReadLine();// đọc dòng tiếp theo trong file
                    }
                    sr.Close(); // đóng streamreder và các dòng bên dưới
                    Console.ReadLine();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception: " + e.Message);
                }
                finally
                {
                    Console.WriteLine("Executing finally block.");
                }
            }

            static void WriteFile(string str)
            {
                try
                {
                    // khai báo đối tượng StreamWriter và truyền vào file cần ghi
                    StreamWriter sw = new StreamWriter("files/write.txt");
                    sw.WriteLine(str); // ghi 1 dấu kết thúc dòng vào chuỗi văn bản
                    sw.Close();// đóng streamWriter và các dòng bên dưới
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception: " + e.Message);
                }
                finally
                {
                    Console.WriteLine("Executing finally block.");
                }
            }
        }
    }