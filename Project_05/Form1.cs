﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_05
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //khai báo đối tượng StreamReader  và truyền vào file cần đọc: textbox1
            StreamWriter writer = new StreamWriter(textBox1.Text);
            // ghi giá trị ở ô textbox2 vào file
            writer.WriteLine(textBox2.Text, false); // true: nếu file chưa tồn tại thì cho phép tạo mới file
            writer.Close(); // Đóng đối tương StreamWriter và các dòng bên dưới
            MessageBox.Show("Ghi vào file thành công !");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            StreamReader reader = new StreamReader(textBox1.Text);// lấy tên file từ textbox 1 để đọc file
            textBox3.Text = reader.ReadToEnd(); // đọc từ đầu tới cuối trong file
            reader.Close();
        }
    }
}
